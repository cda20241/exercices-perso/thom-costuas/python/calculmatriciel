"""
Matrix Operations

This script defines functions to perform various operations on matrices, including creating matrices, changing values, printing matrices, checking dimensions, matrix addition, finding elements, scalar multiplication, and matrix multiplication.

Functions:
- matrice(x, y): Creates a matrix of size x by y filled with zeros.
- change_valeur(x, y, newVal, matrice): Changes the value at position (x, y) in the matrix to newVal.
- print_matrice(matrice): Prints the elements of the matrix.
- dimension_check(matriceX, matriceY): Checks if two matrices have the same dimensions.
- somme_matrice(MX, MY): Adds two matrices.
- is_element(matrice, number): Finds the position of an element in the matrix.
- produit_scalaire(matrice, coef): Multiplies each element of the matrix by a scalar coefficient.
- produit_matriciel(matrice1, matrice2): Performs matrix multiplication.

Usage:
Run the script to see the results of each matrix operation.

Example:
python script_name.py
"""



def matrice(x,y):
    lst = []
    for i in range(0,x):
        newLine=[]
        for j in range(0,y):
            newLine+= [0]
        lst.append(newLine)
    return lst

def change_valeur(x,y, newVal, matrice):
    matrice[x][y] = newVal
    return matrice


def print_matrice(matrice):
    for lines in matrice:
        print(lines)

def dimension_check(matriceX, matriceY):
    Longueur_X = len(matriceX)
    Largeur_X = len(matriceX[0])

    Longueur_Y = len(matriceY)
    Largeur_Y = len(matriceY[0])

    if Longueur_X != Longueur_Y or Largeur_X != Largeur_Y:
        return False
    return True

def somme_matrice(MX,MY):
    if not dimension_check(MX,MY):
        raise Exception("MatriceDimensionError()")
    else:
        newM = matrice(len(MX), len(MX[0]))
        for i in range(len(MX)):
            for j in range(len(MX[0])):
                newM = change_valeur(i, j, MX[i][j] + MY[i][j], newM)
    return newM


def is_element(matrice, number):
    for i in range(len(matrice)):
        for j in range(len(matrice[0])):
            if matrice[i][j] == number:
                return (i,j)


def produit_scalaire(matrice, coef):
    for i in range(len(matrice)):
        for j in range(len(matrice[0])):
            matrice = change_valeur(i,j, matrice[i][j] * coef, matrice)
    return matrice

M = matrice(3,3)
M2 = matrice(3,3)


def produit_matriciel(matrice1, matrice2):
    nb_lignes_m1, nb_colonnes_m1 = len(matrice1), len(matrice1[0])
    nb_lignes_m2, nb_colonnes_m2 = len(matrice2), len(matrice2[0])

    if nb_colonnes_m1 != nb_lignes_m2:
        raise Exception("Les dimensions des matrices ne permettent pas le produit matriciel.")

    newM = matrice(nb_lignes_m1, nb_colonnes_m2)

    for i in range(nb_lignes_m1):
        for j in range(nb_colonnes_m2):
            for k in range(nb_colonnes_m1):
                newM[i][j] += matrice1[i][k] * matrice2[k][j]

    return newM


print(M)
print()

print(change_valeur(1,1, 1, M))
print()


print_matrice(M2)
print()

print(dimension_check(M,M2))
print()


print_matrice(M)
print()
print_matrice(somme_matrice(M,M))
print()


print(is_element(M,1))
print()

print_matrice(produit_scalaire(M,4))
print()


matrice_produit1 = [[2, 2],
                    [2, 2],
                    [2, 2]]

matrice_produit2 = [[1, 1, 1],
                    [1, 1, 1]]

print("Matrice terme 1 : ")
print_matrice(matrice_produit1)
print()

print("Matrice terme 2 : ")
print_matrice(matrice_produit2)
print()

print("Produit matriciel")
print_matrice(produit_matriciel(matrice_produit2, matrice_produit1))
